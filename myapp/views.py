from django.shortcuts import render
from django.http import HttpResponse
import requests

# Create your views here.

def first(rsqt):
    return HttpResponse("<h1 style='color:red; background:skyblue'>Welcome to Django Page</h1>")
def about(rqst1):
    return HttpResponse("<h1>Welcome to second Django page<h1>")

# 28/3/20    
def home(request):
    st = "I am from views.py file"
    list = ["red","yellow","blue","green"]
    context = {"color":list,"str":st}
    return render(request,"home.html",context)

#home task

def api(rqst2):
    url = "https://restcountries.eu/rest/v2/all"
    data =  requests.get(url).json()
    context = {"api":data}
    return render(rqst2,"api.html",context)